package com.kelompok8.remindme.controller;

import com.kelompok8.remindme.core.HelpFlex;
import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.ReminderPackage;
import com.kelompok8.remindme.repository.LineUserRepository;
import com.kelompok8.remindme.service.ReminderPackageServiceImplementation;
import com.kelompok8.remindme.service.ReminderService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.template.CarouselColumn;
import com.linecorp.bot.model.message.template.CarouselTemplate;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
@LineMessageHandler
public class RemindmeController {

  @Autowired
  LineMessagingClient lineMessagingClient;

  @Autowired
  ReminderService reminderService;

  @Autowired
  ReminderPackageServiceImplementation reminderPackageServiceImplementation;

  @Autowired
  LineUserRepository lineUserRepository;

  /**
   * Handle any message that User send to RemindMeBot.
   *
   * @param messageEvent The message event from User.
   */
  @EventMapping
  public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent)
      throws InterruptedException, ExecutionException {
    try {
      String userId = messageEvent.getSource().getUserId();
      String userName = lineMessagingClient.getProfile(userId).get().getDisplayName();
      LineUser lineUser = isUserRegistered(userId, userName);
      Instant timeJakartaZone = messageEvent.getTimestamp().plus(7, ChronoUnit.HOURS);

      String input = messageEvent.getMessage().getText().toLowerCase();
      String[] splitInput = input.split(";");
      String firstWordInput = splitInput[0];
      String answer = "";
      TemplateMessage reminderPackageAnswer = null;
      FlexMessage helpFlex = null;

      switch (firstWordInput) {
        case ("/showmenu"):
          helpFlex = new HelpFlex().get();
          break;
        case ("/addreminder"):
          answer = reminderService.executeAdd("add", splitInput, lineUser, this, timeJakartaZone);
          break;
        case ("/deletereminder"):
          answer = reminderService.executeDelete("delete", splitInput, lineUser);
          break;
        case ("/showreminder"):
          answer = reminderService.executeList("list", splitInput, lineUser);
          break;
        case ("/addbyid"):
          answer = reminderService.addById(splitInput, lineUser, this, timeJakartaZone);
          break;
        case ("/package"):
          long[] arrPackageId = {1, 2, 3, 4, 5};
          List<CarouselColumn> carouselColumnList = makeCarouselColList(arrPackageId);
          CarouselTemplate carouselTemplate = new CarouselTemplate(carouselColumnList);
          reminderPackageAnswer = new TemplateMessage("Reminder Packages", carouselTemplate);
          answer = "Ini lah 5 Reminder Package Rekomendasi kami minggu ini \n\n"
              + "Untuk melihat semua reminder package yang ada, kirim pesan: \n"
              + "'/allpackage'";
          break;
        case ("/allpackage"):
          answer = reminderPackageServiceImplementation.getListReminderPackage()
              + "\nadd package by send \n'/addpackage;<ID>;<YYYY-MM-DD>'";
          break;
        case ("/packagedesc"):
          long reminderPackageId = Long.parseLong(splitInput[1]);
          String reminderPackageDesc = reminderPackageServiceImplementation.getPackageDesc(
              reminderPackageId);
          answer = reminderPackageDesc
              + String.format("\n\nAdd this package by send \n'/addpackage;%d;<YYYY-MM-DD>'",
              reminderPackageId);
          break;
        case ("/addpackage"):
          long reminderPkgId = Long.parseLong(splitInput[1]);
          String date = splitInput[2];
          answer = reminderPackageServiceImplementation.addReminderFromPackage(
              reminderPkgId, lineUser, this, timeJakartaZone, date
          );
          break;
        case ("/snoozereminder"):
          answer = reminderService.executeSnooze(splitInput, lineUser, this, timeJakartaZone);
          break;
        case ("/setreminder"):
          answer = reminderService.setReminder(splitInput, this, timeJakartaZone);
          break;
        default:
          answer = "Sorry, invalid command\n"
              + "Type /showMenu for valid commands";
      }
      String replyToken = messageEvent.getReplyToken();
      if (helpFlex != null) {
        handleReplyFlexEvent(replyToken, helpFlex);
      } else {
        if (reminderPackageAnswer != null) {
          handlePushEventTemplateMessage(userId, reminderPackageAnswer);
        }
        handleReplyEvent(replyToken, answer);
      }
    } catch (NullPointerException | ParseException e) {
      System.out.print("ERROR: " + e.getMessage());
    }
  }


  /**
   * Handle reply message.
   *
   * @param replyToken Token as User identifier.
   * @param answer     Message that will be send to User.
   */
  public void handleReplyEvent(String replyToken, String answer) {
    TextMessage jawabanDalamBentukTextMessage = new TextMessage(answer);
    try {
      lineMessagingClient
          .replyMessage(new ReplyMessage(replyToken, jawabanDalamBentukTextMessage))
          .get();
    } catch (NullPointerException | InterruptedException | ExecutionException e) {
      System.out.print("ERROR: " + e.getMessage() + "\n");
    }
  }

  /**
   * Handle reply message.
   *
   * @param replyToken Token as User identifier.
   * @param answer     Message that will be send to User.
   */
  public void handleReplyFlexEvent(String replyToken, Message answer) {
    try {
      lineMessagingClient
          .replyMessage(new ReplyMessage(replyToken, Collections.singletonList(answer)))
          .get();
    } catch (NullPointerException | InterruptedException | ExecutionException e) {
      System.out.print("ERROR: " + e.getMessage() + "\n");
    }
  }

  /**
   * Handle push message for reminder notification.
   *
   * @param userID  User identifier.
   * @param message Message that will be send to User.
   */
  public void handlePushEvent(String userID, String message) {
    TextMessage jawabanDalamBentukTextMessage = new TextMessage(message);
    try {
      lineMessagingClient
          .pushMessage(new PushMessage(userID, jawabanDalamBentukTextMessage))
          .get();
    } catch (NullPointerException | InterruptedException | ExecutionException e) {
      System.out.print("ERROR: " + e.getMessage() + "\n");
    }
  }

  /**
   * Handle push message for reminder notification with Template Message.
   *
   * @param userID  User identifier.
   * @param message Message that will be send to User.
   */
  public void handlePushEventTemplateMessage(String userID, TemplateMessage message) {
    try {
      lineMessagingClient
          .pushMessage(new PushMessage(userID, message))
          .get();
    } catch (NullPointerException | InterruptedException | ExecutionException e) {
      System.out.print("ERROR: " + e.getMessage() + "\n");
    }
  }

  /**
   * Retrieve Line User from LineUserRepository.
   *
   * @param userID   User's identifier.
   * @param userName User's display name.
   */
  public LineUser isUserRegistered(String userID, String userName) {
    if (lineUserRepository.isUserRegistered(userID)) {
      return lineUserRepository.findLineUserByUserId(userID);
    } else {
      LineUser newUser = new LineUser(userID, userName);
      return lineUserRepository.save(newUser);
    }
  }

  /**
   * Make a list of Carousel Column.
   *
   * @param arrIdPackage array consist of reminder package id.
   */
  public List<CarouselColumn> makeCarouselColList(long[] arrIdPackage) {
    try {
      List<CarouselColumn> carouselColumnList = new ArrayList<CarouselColumn>();
      for (long id : arrIdPackage) {
        Optional<ReminderPackage> reminderPackage =
            reminderPackageServiceImplementation.getReminderPackageById(id);
        String imageUrl = reminderPackage.get().getImageUrl();
        String title = reminderPackage.get().getNama();
        String desc = String.format("Send '/addpackage;%d;<YYYY-MM-DD>' to add this package",
            reminderPackage.get().getId());
        String messageActionText = String.format("/packagedesc;%d", reminderPackage.get().getId());
        CarouselColumn carouselColumn = new CarouselColumn(imageUrl, title, desc,
            Arrays.asList(new MessageAction("See Description", messageActionText)));
        carouselColumnList.add(carouselColumn);
      }
      return carouselColumnList;
    } catch (Exception e) {
      System.out.print("ERROR: " + e.getMessage() + "\n");
      return null;
    }
  }
}
