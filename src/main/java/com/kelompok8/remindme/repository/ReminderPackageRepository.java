package com.kelompok8.remindme.repository;

import com.kelompok8.remindme.core.ReminderPackage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReminderPackageRepository extends JpaRepository<ReminderPackage, Long> {

}