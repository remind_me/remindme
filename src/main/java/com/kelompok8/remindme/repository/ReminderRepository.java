package com.kelompok8.remindme.repository;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ReminderRepository extends JpaRepository<Reminder, Long> {
  @Query(value = "SELECT * FROM REMINDER WHERE NAMA = ?1 AND LINEUSER_ID = ?2", nativeQuery = true)
  Reminder findReminderByName(String nama, LineUser user);

  @Modifying
  @Transactional
  @Query(value = "DELETE FROM REMINDER WHERE id = ?", nativeQuery = true)
  void deleteReminderById(Long id);

  @Modifying
  @Transactional
  @Query(value = "UPDATE REMINDER SET waktu = ?2 WHERE id = ?1", nativeQuery = true)
  void updateTimeById(Long id, Date newDate);

  @Modifying
  @Transactional
  @Query(value = "UPDATE REMINDER SET status = ?2 WHERE id = ?1", nativeQuery = true)
  void updateStateById(Long id, String newState);

  @Query(value = "SELECT * FROM REMINDER WHERE lineuser_id = ?1", nativeQuery = true)
  List<Reminder> findAllReminderByID(LineUser user);
}
