package com.kelompok8.remindme.repository;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.core.command.Command;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sound.sampled.Line;
import org.springframework.stereotype.Repository;

@Repository
public class CommandRepository {
  private Map<String, Command> commands;

  public CommandRepository() {
    commands = new HashMap<>();
  }

  public void newCommand(Command command) {
    commands.put(command.nameCommand(), command);
  }

  public Reminder executeAdd(String nameCommand, String[] list, LineUser user)
      throws ParseException {
    return commands.get(nameCommand).add(list,user);
  }

  public Long executeDelete(String namaCommand, String[] list, LineUser userId) {
    return commands.get(namaCommand).delete(list, userId);
  }

  public String executeList(String namaCommand, String[] list, LineUser userId) {
    return commands.get(namaCommand).list(list, userId);
  }

  public Collection<Command> getCommands() {
    return commands.values();
  }
}
