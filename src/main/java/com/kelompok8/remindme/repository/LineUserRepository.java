package com.kelompok8.remindme.repository;

import com.kelompok8.remindme.core.LineUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LineUserRepository extends JpaRepository<LineUser, String> {
  @Query(value = "SELECT * FROM LINE_USER WHERE USER_ID = ?1", nativeQuery = true)
  LineUser findLineUserByUserId(String userID);

  @Query(value = "SELECT CASE WHEN COUNT(c) > 0 THEN true "
      + "ELSE false END FROM LINE_USER c WHERE c.USER_ID = ?1", nativeQuery = true)
  boolean isUserRegistered(String userID);

}
