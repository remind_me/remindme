package com.kelompok8.remindme.service;

import com.kelompok8.remindme.controller.RemindmeController;
import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.command.Command;
import com.kelompok8.remindme.service.timer.RemindTask;
import java.text.ParseException;
import java.time.Instant;

public interface ReminderService {
  public String executeAdd(String nameCommand, String[] list, LineUser user,
                           RemindmeController controller, Instant waktu) throws ParseException;

  public String executeDelete(String nameCommand, String[] list, LineUser userId);

  public String executeList(String nameCommand,String[] list, LineUser userId);

  public void newCommand(Command command);

  public String addById(String[] list, LineUser user,
                        RemindmeController controller, Instant waktu);

  public String executeSnooze(String[] list, LineUser user,
                              RemindmeController controller, Instant waktu);

  public String setReminder(String[] list, RemindmeController controller, Instant waktu);

  public void newRemindTask(Long reminderID, RemindTask remindTask);
}
