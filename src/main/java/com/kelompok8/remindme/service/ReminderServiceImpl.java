package com.kelompok8.remindme.service;

import com.kelompok8.remindme.controller.RemindmeController;
import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.core.RepeatedState;
import com.kelompok8.remindme.core.UnrepeatedYetState;
import com.kelompok8.remindme.core.command.AddCommand;
import com.kelompok8.remindme.core.command.Command;
import com.kelompok8.remindme.core.command.DeleteCommand;
import com.kelompok8.remindme.core.command.ListCommand;
import com.kelompok8.remindme.repository.CommandRepository;
import com.kelompok8.remindme.repository.ReminderRepository;
import com.kelompok8.remindme.service.timer.RemindTask;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReminderServiceImpl implements ReminderService {

  @Autowired
  private CommandRepository commandRepository;

  @Autowired
  private ReminderRepository reminderRepository;

  private HashMap<Long, RemindTask> remindTasks = new HashMap<>();

  @Override
  public String executeAdd(String nameCommand, String[] list, LineUser user,
                           RemindmeController controller, Instant waktu) {
    try {
      commandRepository.newCommand(new AddCommand());
      Reminder reminder = commandRepository.executeAdd(nameCommand, list, user);
      RemindTask remindTask = new RemindTask(controller,reminder,reminderRepository);
      remindTask.makeRemind(waktu);

      reminderRepository.save(reminder);
      Long idReminder = reminder.getId();
      remindTasks.put(idReminder, remindTask);
      return "Reminder " + reminder.getNama() + " is added with id = " + idReminder;
    } catch (Exception e) {
      return "ERROR: " + e.getMessage();
    }
  }


  @Override
  public String executeDelete(String nameCommand, String[] list, LineUser user) {
    try {
      commandRepository.newCommand(new DeleteCommand());
      Long idReminder = commandRepository.executeDelete(nameCommand, list, user);

      remindTasks.get(idReminder).cancelRemind();
      remindTasks.remove(idReminder);
      reminderRepository.deleteReminderById(idReminder);
      return "Reminder " + idReminder + " is deleted";
    } catch (Exception e) {
      return "Error: " + e.getMessage();
    }
  }

  @Override
  public String executeList(String nameCommand, String[] list, LineUser userId) {
    try {
      commandRepository.newCommand(new ListCommand());
      String namaReminder = commandRepository.executeList(nameCommand, list, userId);
      if (namaReminder.toLowerCase().equals("showall")) {
        List<Reminder> listReminder = reminderRepository.findAllReminderByID(userId);
        StringBuilder allReminder = new StringBuilder();
        allReminder.append("- UPCOMING REMINDERS -\n");
        for (Reminder reminder : listReminder) {
          allReminder.append(reminder.getNama()).append("\nID: ");
          allReminder.append(reminder.getId()).append("\nState:");
          allReminder.append(reminder.getState()).append("\n");
          allReminder.append(reminder.getWaktu()).append("\n\n");
        }
        return allReminder.toString();
      } else {
        Reminder reminder2 = reminderRepository.findReminderByName(namaReminder, userId);
        return reminder2.getNama() + " " + reminder2.getWaktu() + " " + reminder2.getId();
      }
    } catch (Exception e) {
      return "Error: " + e.getMessage();
    }
  }

  @Override
  public String executeSnooze(String[] list, LineUser user,
                              RemindmeController controller, Instant waktu) {
    try {
      Long reminderID = Long.parseLong(list[1]);
      Reminder reminder = reminderRepository.getOne(reminderID);
      DateTime newTime = new DateTime(reminder.getWaktu()).plusMinutes(5);
      DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
      reminder.setWaktu(dateTimeFormatter.print(newTime));
      RemindTask remindTask = new RemindTask(controller,reminder,reminderRepository);
      remindTask.makeRemind(waktu);

      reminderRepository.updateTimeById(reminderID,newTime.toDate());
      newRemindTask(reminderID,remindTask);
      return "Reminder " + reminder.getNama() + " has been postponed to "
          + dateTimeFormatter.print(newTime);
    } catch (Exception e) {
      return "Error: " + e.getMessage();
    }
  }

  @Override
  public void newCommand(Command command) {
    commandRepository.newCommand(command);
  }

  @Override
  public String addById(String[] list, LineUser user,
                        RemindmeController controller, Instant waktu) {
    try {
      Long reminderID = Long.parseLong(list[1]);
      Reminder reminder = reminderRepository.getOne(reminderID);
      String reminderName = reminder.getNama();
      DateTime reminderTime = new DateTime(reminder.getWaktu());
      DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");

      String[] remindList = {"add", reminderName, dtf.print(reminderTime)};
      return executeAdd("add", remindList, user, controller, waktu);
    } catch (Exception e) {
      return "Error: " + e.getMessage();
    }
  }

  public HashMap<Long, RemindTask> getRemindTasks() {
    return remindTasks;
  }

  @Override
  public String setReminder(String[] list, RemindmeController controller, Instant waktu) {
    try {
      Long reminderID = Long.parseLong(list[1]);
      Reminder reminder = reminderRepository.getOne(reminderID);

      String nowState = list[2].toLowerCase();
      String answer;
      switch (nowState) {
        case ("repeat"):
          if (reminder.getState().equals(UnrepeatedYetState.DB_COL_NAME)) {
            UnrepeatedYetState setRemind = new UnrepeatedYetState(reminder,reminderRepository);
            answer = setRemind.repeat();
            reminder = reminderRepository.getOne(reminderID);
            RemindTask remindTask = new RemindTask(controller, reminder,reminderRepository);
            remindTask.makeRemind(waktu);
            newRemindTask(reminderID,remindTask);
            return answer;
          } else {
            RepeatedState setRemind = new RepeatedState(reminder,reminderRepository);
            return setRemind.repeat();
          }
        case ("unrepeat"):
          if (reminder.getState().equals(UnrepeatedYetState.DB_COL_NAME)) {
            UnrepeatedYetState setRemind = new UnrepeatedYetState(reminder,reminderRepository);
            return setRemind.unrepeat();
          } else {
            RepeatedState setRemind = new RepeatedState(reminder,reminderRepository);
            answer = setRemind.unrepeat();
            reminder = reminderRepository.getOne(reminderID);
            RemindTask remindTask = new RemindTask(controller, reminder,reminderRepository);
            newRemindTask(reminderID,remindTask);
            remindTask.makeRemind(waktu);
            return answer;
          }
        default:
          return "Sorry, invalid command. Type /showMenu for valid commands";
      }
    } catch (ArithmeticException e) {
      reminderRepository.deleteReminderById(Long.parseLong(list[1]));
      return "Success! This reminder will not be reminded again.";
    } catch (Exception e) {
      return "Error: " + e.getMessage();
    }
  }

  @Override
  public void newRemindTask(Long reminderID, RemindTask remindTask) {
    remindTasks.get(reminderID).cancelRemind();
    remindTasks.remove(reminderID);
    remindTasks.put(reminderID, remindTask);
  }
}