package com.kelompok8.remindme.service;

import com.kelompok8.remindme.controller.RemindmeController;
import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.ReminderPackage;
import com.kelompok8.remindme.repository.ReminderPackageRepository;
import java.time.Instant;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReminderPackageServiceImplementation implements ReminderPackageService {

  @Autowired
  private ReminderPackageRepository reminderPackageRepository;

  @Autowired
  ReminderServiceImpl reminderService;

  @Override
  public String addReminderFromPackage(long idReminderPackage, LineUser user,
                                       RemindmeController controller, Instant waktu,
                                       String reminderDate) {
    try {
      Optional<ReminderPackage> reminderPkg = reminderPackageRepository.findById(idReminderPackage);
      String[] lstReminderInPkg = reminderPkg.get().getListReminder().split("; ");
      String namaPackage = reminderPkg.get().getNama();
      for (String reminder : lstReminderInPkg) {
        String[] reminderDesc = reminder.split(", ");
        String reminderName = reminderDesc[0];
        String reminderTime = reminderDesc[1];
        String reminderDateTime = reminderDate + " " + reminderTime;
        String[] input = {"/addreminder", reminderName, reminderDateTime};
        reminderService.executeAdd("add", input, user, controller, waktu);
      }
      return String.format("Berhasil menambahkan %s!", namaPackage);
    } catch (NullPointerException e) {
      return "Package tidak dapat ditemukan.";
    } catch (ArrayIndexOutOfBoundsException e) {
      return "Ada kesalahan pada package.";
    }
  }

  /**
   * Description of the Package.
   *
   * @return reminder of the Package.
   */
  public String getPackageDesc(long idReminderPackage) {
    Optional<ReminderPackage> reminderPkg = reminderPackageRepository.findById(idReminderPackage);
    String[] lstReminderInPkg = reminderPkg.get().getListReminder().split("; ");
    String namaPackage = reminderPkg.get().getNama();
    String reminderDesc = "";
    String packageDesc = String.format("%s\n\nList reminder:\n", namaPackage);
    int numberOfReminderInPkg = lstReminderInPkg.length;
    int counterReminder = 1;
    for (String reminder : lstReminderInPkg) {
      reminderDesc = getReminderDesc(reminder, counterReminder, numberOfReminderInPkg);
      counterReminder++;
      packageDesc += reminderDesc;
    }
    return packageDesc;
  }

  /**
   * Method to check is the Reminder is the last Reminder in Package.
   *
   * @return boolean.
   */
  public boolean isLastReminderInPackage(int counterReminder, int numberOfReminderInPkg) {
    if (counterReminder == numberOfReminderInPkg) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Method to get Reminder description from Package.
   *
   * @return description of Reminder.
   */
  public String getReminderDesc(String reminder, int counterReminder, int numberOfReminderInPkg) {
    String[] listOfReminderDesc = reminder.split(", ");
    if (!(isLastReminderInPackage(counterReminder, numberOfReminderInPkg))) {
      return String.format("%s. %s - %s\n",
              Integer.toString(counterReminder), listOfReminderDesc[1], listOfReminderDesc[0]);
    } else {
      return String.format("%s. %s - %s",
              Integer.toString(counterReminder), listOfReminderDesc[1], listOfReminderDesc[0]);
    }
  }

  public Optional<ReminderPackage> getReminderPackageById(long id) {
    return reminderPackageRepository.findById(id);
  }

  /**
   * Method to get list of all reminder package.
   *
   * @return list of all reminder package.
   */
  public String getListReminderPackage() {
    String listReminderPackage = "List Reminder Package:\n";
    int counter = 0;
    for (ReminderPackage reminderPackage : reminderPackageRepository.findAll()) {
      listReminderPackage += String.format("%d. %s (ID = %d)\n",
              ++counter,
              reminderPackage.getNama(),
              reminderPackage.getId());
    }
    return listReminderPackage;
  }
}
