package com.kelompok8.remindme.service.timer;

import com.kelompok8.remindme.core.Reminder;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class RemindTimer implements Supplier<CompletableFuture<String>> {
  private Reminder reminder;
  String answer;

  public RemindTimer(Reminder reminder) {
    this.reminder = reminder;
    this.answer = "! REMINDER: " + this.reminder.getNama().toUpperCase() + " !";
  }

  @Override
  public CompletableFuture<String> get() {
    return CompletableFuture.completedFuture(answer);
  }
}