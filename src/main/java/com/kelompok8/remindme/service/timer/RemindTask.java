package com.kelompok8.remindme.service.timer;

import com.kelompok8.remindme.controller.RemindmeController;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.repository.ReminderRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RemindTask {

  private RemindmeController controller;
  private Reminder reminder;
  private ScheduledExecutorService scheduledExecutorService;
  private ReminderRepository reminderRepository;
  long jumlahRemindMessage;
  long delay;

  /**
   * Constructor for RemindTask.
   *
   * @param controller Controller to send message notifications.
   * @param reminder   The reminder that wanted to be sent.
   */
  public RemindTask(RemindmeController controller, Reminder reminder,
                    ReminderRepository reminderRepository) {
    this.controller = controller;
    this.reminder = reminder;
    this.jumlahRemindMessage = 0;
    this.delay = 0;
    this.reminderRepository = reminderRepository;
  }

  /**
   * Schedule to send message to User.
   *
   * @param waktuDibuatReminder Time the reminder was made.
   */
  public void makeRemind(Instant waktuDibuatReminder) throws ArithmeticException {
    Instant waktuRemind = this.reminder.getWaktu().toInstant();
    delay = ChronoUnit.MILLIS.between(waktuDibuatReminder, waktuRemind);
    if (delay >= 0) {
      this.scheduledExecutorService = Executors.newScheduledThreadPool(3);
      RemindTimer asyncTask = new RemindTimer(reminder);

      if (this.reminder.getState().equals("REPEATED")) {
        this.scheduledExecutorService.scheduleAtFixedRate(() -> {
              controller.handlePushEvent(this.reminder.getLineUser().getUserId(), asyncTask.answer);
            },
            delay,
            TimeUnit.HOURS.toMillis(1),
            TimeUnit.MILLISECONDS
        );
      } else {
        CompletableFuture<String> future = ScheduledCompletable
            .schedule(scheduledExecutorService, asyncTask, delay);
        future.thenAccept(answer -> {
          controller.handlePushEvent(this.reminder.getLineUser().getUserId(), answer);
          cancelRemind();
          reminderRepository.deleteReminderById(this.reminder.getId());
        });
      }

      jumlahRemindMessage++;
    } else {
      throw new ArithmeticException("The due date of the reminder is in the past");
    }
  }

  public void cancelRemind() {
    this.scheduledExecutorService.shutdownNow();
  }

  public ScheduledExecutorService getScheduledExecutorService() {
    return scheduledExecutorService;
  }
}