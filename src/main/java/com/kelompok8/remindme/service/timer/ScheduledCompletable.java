package com.kelompok8.remindme.service.timer;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class ScheduledCompletable {

  /**
   * Executor for schedule reminder.
   *
   * @param executor Scheduled executor (Thread Pool).
   * @param command  Task to be executed.
   * @param delay    Delay as the time to execute.
   */
  public static CompletableFuture<String> schedule(
      ScheduledExecutorService executor,
      Supplier<CompletableFuture<String>> command,
      long delay
  ) {
    CompletableFuture<String> completableFuture = new CompletableFuture<>();
    executor.schedule(
        (() -> {
          command.get().thenAccept(
              t -> {
                completableFuture.complete(t);
              }
          )
              .exceptionally(
                  t -> {
                    completableFuture.completeExceptionally(t);
                    return null;
                  });
        }),
        delay,
        TimeUnit.MILLISECONDS
    );
    return completableFuture;
  }
}