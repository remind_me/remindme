package com.kelompok8.remindme.service;

import com.kelompok8.remindme.controller.RemindmeController;
import com.kelompok8.remindme.core.LineUser;
import java.time.Instant;

public interface ReminderPackageService {
  public String addReminderFromPackage(long idReminderPackage, LineUser user,
                                       RemindmeController controller, Instant waktu,
                                       String reminderDate);
}