package com.kelompok8.remindme.core;

import com.kelompok8.remindme.repository.ReminderRepository;
import org.springframework.stereotype.Component;

@Component
public class RepeatedState implements ReminderState {
  public static final String DB_COL_NAME = "REPEATED";
  private Reminder set;
  private ReminderRepository reminderRepository;

  public RepeatedState() {}

  public RepeatedState(Reminder set, ReminderRepository reminderRepository) {
    this.set = set;
    this.reminderRepository = reminderRepository;
  }

  @Override
  public String repeat() {
    return "This reminder has already been set up as Repeated";
  }

  @Override
  public String unrepeat() {
    set.setState(UnrepeatedYetState.DB_COL_NAME);
    reminderRepository.updateStateById(set.getId(),UnrepeatedYetState.DB_COL_NAME);
    return "Success! This reminder will only be reminded once.";
  }

  @Override
  public String toString() {
    return "Repeated Reminder";
  }
}