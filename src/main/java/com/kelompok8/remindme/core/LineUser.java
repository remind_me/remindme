package com.kelompok8.remindme.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "line_user")
public class LineUser extends AuditModel {
  @Id
  @Column(name = "user_id", unique = true, nullable = false)
  private String userId;

  @Column(name = "username")
  private String userName;

  public LineUser(String userId, String userName) {
    this.userId = userId;
    this.userName = userName;
  }

  public LineUser(){ }

  public String getUserId() {
    return userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }
}
