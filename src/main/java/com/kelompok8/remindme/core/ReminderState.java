package com.kelompok8.remindme.core;

public interface ReminderState {
  String repeat();

  String unrepeat();
}