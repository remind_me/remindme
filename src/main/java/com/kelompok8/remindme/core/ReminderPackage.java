package com.kelompok8.remindme.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "package")
public class ReminderPackage {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "nama")
  private String nama;

  @Column(name = "list_reminder")
  private String listReminder;

  @Column(name = "image_url")
  private String imageUrl;

  /**
   * Default Constructor of ReminderPackage.
   */
  public ReminderPackage() {}

  /**
   * Constructor of ReminderPackage.
   */
  public ReminderPackage(String nama, String listReminder, String imageUrl) {
    this.nama = nama;
    this.listReminder = listReminder;
    this.imageUrl = imageUrl;
  }

  public long getId() {
    return id;
  }

  public String getNama() {
    return nama;
  }

  public String getListReminder() {
    return listReminder;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setNama(String nama) {
    this.nama = nama;
  }

  public void setListReminder(String list) {
    this.listReminder = list;
  }


}