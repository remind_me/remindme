package com.kelompok8.remindme.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StateHelper {
  @Autowired
  private RepeatedState repeatedState;

  @Autowired
  private UnrepeatedYetState unrepeatedYetState;

  /**
   * Get the state from string.
   * @param stateString the original string.
   */
  public ReminderState toState(String stateString) {
    switch (stateString) {
      case RepeatedState.DB_COL_NAME:
        return repeatedState;
      case UnrepeatedYetState.DB_COL_NAME:
        return unrepeatedYetState;
      default:
        return null;
    }
  }
}
