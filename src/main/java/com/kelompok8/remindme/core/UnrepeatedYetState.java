package com.kelompok8.remindme.core;

import com.kelompok8.remindme.repository.ReminderRepository;
import org.springframework.stereotype.Component;

@Component
public class UnrepeatedYetState implements ReminderState {
  public static final String DB_COL_NAME = "UNREPEATED_YET";
  private Reminder set;
  private ReminderRepository reminderRepository;

  public UnrepeatedYetState() {}

  public UnrepeatedYetState(Reminder set, ReminderRepository reminderRepository) {
    this.set = set;
    this.reminderRepository = reminderRepository;
  }

  @Override
  public String repeat() {
    set.setState(RepeatedState.DB_COL_NAME);
    reminderRepository.updateStateById(set.getId(),RepeatedState.DB_COL_NAME);
    return "Success! This reminder will be reminded once every hour.";
  }

  @Override
  public String unrepeat() {
    return "This reminder has already been set up as Unrepeated";
  }

  @Override
  public String toString() {
    return "Unrepeated Reminder";
  }
}