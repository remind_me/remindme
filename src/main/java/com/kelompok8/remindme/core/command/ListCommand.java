package com.kelompok8.remindme.core.command;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.repository.ReminderRepository;
import java.text.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ListCommand implements Command {

  @Autowired
  private ReminderRepository reminderRepository;
  private LineUser lineUser;

  public ListCommand() {
  }

  @Override
  public Reminder add(String[] list, LineUser userId) throws ParseException {
    //Do Nothing
    return null;
  }

  @Override
  public Long delete(String[] namaReminder, LineUser userId) {
    //Do Nothing
    return null;
  }

  @Override
  public String list(String[] list, LineUser userId) {
    if (list[1].toLowerCase().equals("showall")) {
      return "showall";
    } else {
      return list[1];
    }
  }

  @Override
  public String nameCommand() {
    return "list";
  }

}