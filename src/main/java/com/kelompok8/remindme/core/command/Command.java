package com.kelompok8.remindme.core.command;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import java.text.ParseException;
import java.util.List;

// Command for reminder
public interface Command {

  public Reminder add(String[] list, LineUser user) throws ParseException;

  public Long delete(String[] list, LineUser userId);

  public String list(String[] list, LineUser userId);

  public String nameCommand();

}
