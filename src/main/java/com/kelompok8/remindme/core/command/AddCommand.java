package com.kelompok8.remindme.core.command;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.repository.ReminderRepository;
import java.text.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddCommand implements Command {

  public AddCommand() {
  }

  @Override
  public Reminder add(String[] list, LineUser user) throws ParseException {
    // TODO Auto-generated method stub
    Reminder baru = new Reminder(list[1], list[2]);
    baru.setLineUser(user);
    return baru;
  }

  @Override
  public Long delete(String[] namaReminder, LineUser userId) {
    //Do Nothing
    return null;
  }

  @Override
  public String list(String[] list, LineUser userId) {
    //Do Nothing
    return null;
  }

  @Override
  public String nameCommand() {
    return "add";
  }

}
