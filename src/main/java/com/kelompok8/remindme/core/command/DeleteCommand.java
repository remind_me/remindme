package com.kelompok8.remindme.core.command;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.repository.ReminderRepository;
import java.text.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class DeleteCommand implements Command {

  private Reminder reminder;
  @Autowired
  private ReminderRepository reminderRepository;

  public DeleteCommand() {

  }

  @Override
  public Reminder add(String[] list, LineUser userId) throws ParseException {
    //Do Nothing
    return null;
  }

  @Override
  public Long delete(String[] namaReminder, LineUser userId) {
    Long id = Long.parseLong(namaReminder[1]);
    return id;
  }

  @Override
  public String list(String[] list, LineUser userId) {
    //Do Nothing
    return null;
  }

  @Override
  public String nameCommand() {
    return "delete";
  }
}