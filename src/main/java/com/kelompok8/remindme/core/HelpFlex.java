package com.kelompok8.remindme.core;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class HelpFlex implements Supplier<FlexMessage> {
  @Override
  public FlexMessage get() {
    final Box bodyBlock = bodyBlock();
    final Bubble bubble =
        Bubble.builder()
            .body(bodyBlock)
            .build();
    return new FlexMessage("Help", bubble);
  }

  private Box bodyBlock() {
    List<FlexComponent> bodyContents = new ArrayList<>();

    bodyContents.add(
        Text.builder()
            .text("RemindMe Menu")
            .size(FlexFontSize.XL)
            .align(FlexAlign.CENTER)
            .gravity(FlexGravity.CENTER)
            .weight(TextWeight.BOLD)
            .color("#283592")
            .build());

    bodyContents.add(
        Separator.builder()
            .color("#000000")
            .build());

    bodyContents.add(generateHeaderText("/addReminder;<Reminder Name>;<Date> <Hour>"));
    bodyContents.add(generateInfoText("Create a new reminder"));
    bodyContents.add(generateExampleText("ex: /addReminder;Study;2020-04-28 08:00"));

    bodyContents.add(generateHeaderText("/deleteReminder;<Reminder ID>"));
    bodyContents.add(generateInfoText("Delete a reminder by its ID"));
    bodyContents.add(generateExampleText("ex: /deleteReminder;22"));

    bodyContents.add(generateHeaderText("/showReminder;<Reminder Name>"));
    bodyContents.add(generateInfoText("Show the time and ID of a Reminder by its Name"));
    bodyContents.add(generateExampleText("ex: /showReminder;Study"));

    bodyContents.add(generateHeaderText("/showReminder;showall"));
    bodyContents.add(generateInfoText("Show all reminders that has been made"));
    bodyContents.add(generateExampleText("ex: /showReminder;showall"));

    bodyContents.add(generateHeaderText("/snoozeReminder;<Reminder ID>"));
    bodyContents.add(generateInfoText("Delay a Reminder's time to 5 minutes after"));
    bodyContents.add(generateExampleText("ex: /snoozeReminder;22"));

    bodyContents.add(generateHeaderText("/setReminder;<Reminder ID>;<repeat/unrepeat>"));
    bodyContents.add(generateInfoText("Set a Reminder to repeat/unrepeat. If the state is repeat,"
        + " the reminder will be reminded once every hour."));
    bodyContents.add(generateExampleText("ex: /setReminder;22;repeat"));

    bodyContents.add(generateHeaderText("/addById;<Reminder ID>"));
    bodyContents.add(generateInfoText("Create a new reminder with an ID from another user"));
    bodyContents.add(generateExampleText("ex: /addById;22"));

    bodyContents.add(generateHeaderText("/package"));
    bodyContents.add(generateInfoText("Show recommended Reminder Packages"));

    bodyContents.add(generateHeaderText("/allpackage"));
    bodyContents.add(generateInfoText("Show all Reminder Packages that's available"));

    return Box.builder()
        .layout(FlexLayout.VERTICAL)
        .spacing(FlexMarginSize.SM)
        .contents(bodyContents)
        .build();
  }

  private Text generateHeaderText(String name) {
    return Text.builder()
        .text(name)
        .margin(FlexMarginSize.LG)
        .size(FlexFontSize.Md)
        .align(FlexAlign.START)
        .gravity(FlexGravity.CENTER)
        .weight(TextWeight.BOLD)
        .color("#27B1AE")
        .wrap(true)
        .build();
  }

  private Text generateInfoText(String name) {
    return Text.builder()
        .text(name)
        .size(FlexFontSize.SM)
        .align(FlexAlign.START)
        .gravity(FlexGravity.CENTER)
        .weight(TextWeight.REGULAR)
        .color("#000000")
        .wrap(true)
        .build();
  }

  private Text generateExampleText(String name) {
    return Text.builder()
        .text(name)
        .size(FlexFontSize.XS)
        .align(FlexAlign.START)
        .gravity(FlexGravity.CENTER)
        .weight(TextWeight.REGULAR)
        .color("#767676")
        .wrap(true)
        .build();
  }
}