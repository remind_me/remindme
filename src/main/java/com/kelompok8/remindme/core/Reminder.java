package com.kelompok8.remindme.core;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "reminder")
public class Reminder extends AuditModel {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "nama")
  private String nama;

  @Column(name = "waktu")
  private Date waktu;

  @Column(name = "status")
  private String state;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "lineuser_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private LineUser lineUser;

  /**
   * Default Constructor for Reminder.
   */
  public Reminder() { }

  /**
   * Constructor for Reminder.
   *
   * @param nama   The reminder's name.
   * @param waktu  Time to set the reminder.
   */
  public Reminder(String nama, String waktu) throws ParseException {
    this.nama = nama;
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    Date date = dateFormatter.parse(waktu);
    this.waktu = date;
    this.state = UnrepeatedYetState.DB_COL_NAME;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNama() {
    return nama;
  }

  public void setNama(String nama) {
    this.nama = nama;
  }

  public Date getWaktu() {
    return waktu;
  }

  /**
   * Change the time of the reminder.
   *
   * @param waktu New time to set the reminder.
   */
  public void setWaktu(String waktu) throws ParseException {
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    Date date = dateFormatter.parse(waktu);
    this.waktu = date;
  }

  public LineUser getLineUser() {
    return lineUser;
  }

  public void setLineUser(LineUser lineUser) {
    this.lineUser = lineUser;
  }

  public String getState() {
    return this.state;
  }

  public void setState(String settingState) {
    this.state = settingState;
  }

  @Override
  public String toString() {
    return "Sekarang " + this.state + ".";
  }
}