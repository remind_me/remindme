package com.kelompok8.remindme.repository;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.command.AddCommand;
import com.kelompok8.remindme.core.command.Command;
import com.kelompok8.remindme.core.command.DeleteCommand;
import com.kelompok8.remindme.core.command.ListCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


import java.text.ParseException;
import java.util.Collection;

public class CommandRepositoryTest {
  private CommandRepository commandRepository;
  private LineUser lineUser;

  private String[] list = {"/add", "Makan", "2020-02-13 10:00"};
  private String userId = "test";

  @BeforeEach
  public void setUp(){
    commandRepository = new CommandRepository();
    lineUser = new LineUser("000","nama");
  }

  @Test
  public void testNewCommandShouldAddItToTheRepository() {
    Command newCommand = new AddCommand();
    commandRepository.newCommand(newCommand);

    Collection<Command> repositoryCommand = commandRepository.getCommands();

    assertThat(repositoryCommand).hasSize(1);
    assertThat(repositoryCommand).contains(newCommand);
  }

  @Test
  public void testExecuteAddShouldBeCastedFromRepository() throws ParseException {
    Command mockedCommand = mock(AddCommand.class);

    commandRepository.newCommand(mockedCommand);
    commandRepository.executeAdd(mockedCommand.nameCommand(),list,lineUser);

    verify(mockedCommand).add(list, lineUser);
  }

  @Test
  public void testExecuteDeleteShouldBeCastedFromRepository() throws ParseException {
    Command mockedCommand = mock(DeleteCommand.class);

    commandRepository.newCommand(mockedCommand);
    String[] list = {"makan","minum"};
    commandRepository.executeAdd(mockedCommand.nameCommand(),list,lineUser);
    commandRepository.executeDelete(mockedCommand.nameCommand(), list,lineUser);

    verify(mockedCommand).delete(list, lineUser);
  }

  @Test
  public void testExecuteListShouldBeCastedFromRepository() throws ParseException {
    Command mockedCommand = mock(ListCommand.class);

    commandRepository.newCommand(mockedCommand);
    String[] list = {"makan","minum"};
    commandRepository.executeAdd(mockedCommand.nameCommand(),list,lineUser);
    commandRepository.executeList(mockedCommand.nameCommand(),list, lineUser);

    verify(mockedCommand).list(list, lineUser);
  }
}
