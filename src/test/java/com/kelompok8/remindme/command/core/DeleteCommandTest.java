package com.kelompok8.remindme.command.core;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.core.command.AddCommand;
import com.kelompok8.remindme.core.command.DeleteCommand;
import com.kelompok8.remindme.repository.ReminderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class DeleteCommandTest {

  private Class<?> deleteCommandClass;

  @Mock
  private ReminderRepository reminderRepository;
  @InjectMocks
  private DeleteCommand command;
  @Mock
  private Reminder reminder;
  private String[] list = {"add", "Makan", "2020-02-13 10:00"};
  private String[] list2 = {"delete", "1"};
  private String userId = "test";
  private LineUser lineUser;

  @BeforeEach
  public void setUp() throws Exception {
    lineUser = new LineUser("userID","nama");
    deleteCommandClass = Class.forName("com.kelompok8.remindme.core.command.DeleteCommand");
    reminder = new Reminder("Makan", "2020-10-10 10:00");
    reminder.setLineUser(lineUser);
    reminder.setId(1);
  }

  @Test
  public void testDeleteCommandIsConcreteClass() {
    assertFalse(Modifier.
            isAbstract(deleteCommandClass.getModifiers()));
  }

  @Test
  public void testDeleteCommandIsACommand() {
    Collection<Type> interfaces = Arrays.asList(deleteCommandClass.getInterfaces());

    assertTrue(interfaces.stream()
            .anyMatch(type -> type.getTypeName()
                    .equals("com.kelompok8.remindme.core.command.Command")));
  }

  @Test
  public void testDeleteCommandOverrideAddMethod() throws Exception {
    Method add = deleteCommandClass.getDeclaredMethod("add", String[].class ,LineUser.class);

    assertNull(command.add(list, lineUser));


    assertEquals(2, add.getParameterCount());
    assertTrue(Modifier.isPublic(add.getModifiers()));
  }

  @Test
  public void testDeleteCommandOverrideDeleteMethod() throws Exception {
    Method delete = deleteCommandClass.getDeclaredMethod("delete",String[].class,LineUser.class);

    Long hasil = command.delete(list2, lineUser);
    Long baru = Long.parseLong(list2[1]);
    assertEquals(hasil, baru);
    assertEquals("java.lang.Long", delete.getGenericReturnType().getTypeName());

    assertEquals(2, delete.getParameterCount());
    assertTrue(Modifier.isPublic(delete.getModifiers()));
  }

  @Test
  public void testDeleteCommandOverridelistMethod() throws Exception {
    Method executeList = deleteCommandClass.getDeclaredMethod("list",String[].class, LineUser.class);

    assertNull(command.list(list, lineUser));

    assertEquals(2, executeList.getParameterCount());
    assertTrue(Modifier.isPublic(executeList.getModifiers()));
  }

  @Test
  public void testDeleteCommandOverrideNameCommandMethod() throws Exception {
    Method nameCommand = deleteCommandClass.getDeclaredMethod("nameCommand");

    assertEquals("delete", command.nameCommand());
    assertEquals("java.lang.String", nameCommand.getGenericReturnType().getTypeName());
    assertEquals(0, nameCommand.getParameterCount());
  }

}