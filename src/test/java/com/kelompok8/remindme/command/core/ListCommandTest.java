package com.kelompok8.remindme.command.core;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.core.command.DeleteCommand;
import com.kelompok8.remindme.core.command.ListCommand;
import com.kelompok8.remindme.repository.ReminderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


@ExtendWith(MockitoExtension.class)
public class ListCommandTest {

  private Class<?> listCommandClass;

  @Mock
  private ReminderRepository reminderRepository;
  @InjectMocks
  private ListCommand command;
  @Mock
  private Reminder reminder;
  private String[] list = {"add", "makan"};
  private String[] list2 = {"add", "showall"};
  private String userId = "test";
  private LineUser lineUser;

  @BeforeEach
  public void setUp() throws Exception {
    listCommandClass = Class.forName("com.kelompok8.remindme.core.command.ListCommand");
    lineUser = new LineUser("userID","nama");
    reminder = new Reminder("Makan", "2020-10-10 10:00");
    reminder.setLineUser(lineUser);
    reminder.setId(0);
  }

  @Test
  public void testListCommandIsConcreteClass() {
    assertFalse(Modifier.
            isAbstract(listCommandClass.getModifiers()));
  }

  @Test
  public void testListCommandIsACommand() {
    Collection<Type> interfaces = Arrays.asList(listCommandClass.getInterfaces());

    assertTrue(interfaces.stream()
            .anyMatch(type -> type.getTypeName()
                    .equals("com.kelompok8.remindme.core.command.Command")));
  }

  @Test
  public void testDeleteCommandOverrideAddMethod() throws Exception {
    Method add = listCommandClass.getDeclaredMethod("add", String[].class ,LineUser.class);

    assertNull(command.add(list, lineUser));

    assertEquals(2, add.getParameterCount());
    assertTrue(Modifier.isPublic(add.getModifiers()));
  }

  @Test
  public void testAddCommandOverrideDeleteMethod() throws Exception {
    Method add = listCommandClass.getDeclaredMethod("delete",String[].class,LineUser.class);

    assertNull(command.delete(list, lineUser));

    assertEquals(2, add.getParameterCount());
    assertTrue(Modifier.isPublic(add.getModifiers()));
  }

  @Test
  public void testlistCommandOverridelistMethod() throws Exception {
    Method executeList = listCommandClass.getDeclaredMethod("list",String[].class, LineUser.class);

//    reminderRepository.save(reminder);
    String showReminder = command.list(list, lineUser);
//    lenient().when(command.list(list, lineUser)).thenReturn(showReminder);

      assertEquals(list[1].toLowerCase(), showReminder.toLowerCase());


    assertEquals("java.lang.String", executeList.getGenericReturnType().getTypeName());
    assertEquals(2, executeList.getParameterCount());
    assertTrue(Modifier.isPublic(executeList.getModifiers()));
  }

  @Test
  public void testlistCommandOverridelistMethodShowAll() throws Exception {
    Method executeList = listCommandClass.getDeclaredMethod("list",String[].class, LineUser.class);

//    reminderRepository.save(reminder);
    String showReminder = command.list(list2, lineUser);
//    lenient().when(command.list(list, lineUser)).thenReturn(showReminder);

    assertEquals("showall", showReminder.toLowerCase());


    assertEquals("java.lang.String", executeList.getGenericReturnType().getTypeName());
    assertEquals(2, executeList.getParameterCount());
    assertTrue(Modifier.isPublic(executeList.getModifiers()));
  }

  @Test
  public void testAddCommandOverrideNameCommandMethod() throws Exception {
    Method nameCommand = listCommandClass.getDeclaredMethod("nameCommand");
    assertEquals("list", command.nameCommand());
    assertEquals("java.lang.String", nameCommand.getGenericReturnType().getTypeName());
    assertEquals(0, nameCommand.getParameterCount());
  }
}