//package com.kelompok8.remindme.command.core;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//import java.lang.reflect.Modifier;
//import java.lang.reflect.Method;
//
//
//
//public class CommandTest {
//
//  private Class<?> commandClass;
//
//  @BeforeEach
//  public void setup() throws Exception {
//    commandClass = Class.forName("com.kelompok8.remindme.core.command.Command");
//  }
//
//  @Test
//  public void testCommandIsAPublicInterface() {
//    int classModifiers = commandClass.getModifiers();
//
//    assertTrue(Modifier.isPublic(classModifiers));
//    assertTrue(Modifier.isInterface(classModifiers));
//  }
//
//  @Test
//  public void testCommandHasListAbstractMethod() throws Exception {
//    Method cast = commandClass.getDeclaredMethod("list",String.class);
//    int methodModifiers = cast.getModifiers();
//
//    assertTrue(Modifier.isPublic(methodModifiers));
//    assertTrue(Modifier.isAbstract(methodModifiers));
//    assertEquals(1, cast.getParameterCount());
//  }
//
//  @Test
//  public void testCommandHasNameCommandAbstractMethod() throws Exception {
//    Method cast = commandClass.getDeclaredMethod("nameCommand");
//    int methodModifiers = cast.getModifiers();
//
//    assertTrue(Modifier.isPublic(methodModifiers));
//    assertTrue(Modifier.isAbstract(methodModifiers));
//    assertEquals(0, cast.getParameterCount());
//  }
//}