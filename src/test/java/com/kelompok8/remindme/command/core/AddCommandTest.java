package com.kelompok8.remindme.command.core;

import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.core.command.AddCommand;
import com.kelompok8.remindme.core.command.Command;
import com.kelompok8.remindme.repository.ReminderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.Null;
import org.mockito.junit.jupiter.MockitoExtension;
//import org.mockito.runners.MockitoJUnitRunner;


import static org.junit.Assume.assumeNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class AddCommandTest {

  private Class<?> addCommandClass;
  @Mock
  private ReminderRepository reminderRepository;
  @InjectMocks
  private AddCommand command;
  @Mock
  private Reminder reminder;
  private String[] list = {"/add", "Makan", "2020-02-13 10:00"};
  private String userId = "test";

  private LineUser lineUser;

  @BeforeEach
  public void setUp() throws Exception {
    addCommandClass = Class.forName("com.kelompok8.remindme.core.command.AddCommand");
    lineUser = new LineUser(userId,"nama");
    reminder = new Reminder("Makan", "2020-10-10 10:00");
    reminder.setLineUser(lineUser);
    reminder.setId(0);
  }

  @Test
  public void testAddCommandIsConcreteClass() {
    assertFalse(Modifier.
            isAbstract(addCommandClass.getModifiers()));
  }

  @Test
  public void testAddCommandIsACommand() {
    Collection<Type> interfaces = Arrays.asList(addCommandClass.getInterfaces());

    assertTrue(interfaces.stream()
            .anyMatch(type -> type.getTypeName()
                    .equals("com.kelompok8.remindme.core.command.Command")));
  }

  @Test
  public void testAddCommandOverrideAddMethod() throws Exception {
    Method add = addCommandClass.getDeclaredMethod("add", String[].class ,LineUser.class);
    Reminder baru = command.add(list,lineUser);
    assertEquals("com.kelompok8.remindme.core.Reminder", add.getGenericReturnType().getTypeName());
    assertEquals(2, add.getParameterCount());
    assertTrue(Modifier.isPublic(add.getModifiers()));
  }

  @Test
  public void testAddCommandOverrideDeleteMethod() throws Exception {
    Method delete = addCommandClass.getDeclaredMethod("delete",String[].class,LineUser.class);

    assertNull(command.delete(list,lineUser));

    assertEquals(2, delete.getParameterCount());
    assertTrue(Modifier.isPublic(delete.getModifiers()));
  }

  @Test
  public void testAddCommandOverridelistMethod() throws Exception {
    Method executeList = addCommandClass.getDeclaredMethod("list",String[].class, LineUser.class);

    assertNull(command.list(list, lineUser));

    assertEquals(2, executeList.getParameterCount());
    assertTrue(Modifier.isPublic(executeList.getModifiers()));
  }

  @Test
  public void testAddCommandOverrideNameCommandMethod() throws Exception {
    Method nameCommand = addCommandClass.getDeclaredMethod("nameCommand");
  assertEquals("add", command.nameCommand());
    assertEquals("java.lang.String", nameCommand.getGenericReturnType().getTypeName());
    assertEquals(0, nameCommand.getParameterCount());
  }

}