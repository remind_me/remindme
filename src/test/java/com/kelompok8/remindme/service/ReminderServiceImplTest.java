package com.kelompok8.remindme.service;

import com.kelompok8.remindme.controller.RemindmeController;
import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.core.command.AddCommand;
import com.kelompok8.remindme.core.command.Command;
import com.kelompok8.remindme.core.command.DeleteCommand;
import com.kelompok8.remindme.core.command.ListCommand;
import com.kelompok8.remindme.repository.CommandRepository;
import com.kelompok8.remindme.repository.ReminderRepository;
import com.kelompok8.remindme.service.timer.ScheduledCompletable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ReminderServiceImplTest {

  @InjectMocks
  private ReminderServiceImpl reminderService;

  @Mock
  private CommandRepository commandRepository;

  @Mock
  private ReminderRepository reminderRepository;

  @Mock
  private Reminder reminder;

  @Mock
  private RemindmeController controller;

  @Mock
  private ScheduledExecutorService scheduledExecutorService;

  @Mock
  private ScheduledCompletable scheduledCompletable;

  private Map<String, Command> commands = new HashMap<>();

  String[] listAdd = {"add","makan","2020-04-03 10:00"};
  String[] listListAll = {"/showreminder","showall"};
  String[] listList = {"/showreminder","makan","2020-04-03 10:00"};
  String[] listAddById = {"/addbyid", "1"};
  String[] listSetReminder = {"/setreminder", "1", "repeat"};
  private Instant waktu;
  private LineUser lineUser;

  @BeforeEach
  public void setUp() throws ParseException {
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    Date defaultDate = dateFormatter.parse("2020-05-11 15:30");
    this.waktu = defaultDate.toInstant();

    this.lineUser = new LineUser("userId","userName");
  }

  @AfterEach
  void tearDown() {
    reset(reminderRepository);
  }

  @Test
  public void whenExecuteAddIsCalledItShouldCallCommandRepositoryExecuteAdd() throws ParseException {
    Command command = new AddCommand();

    Reminder reminder = new Reminder("makan","2020-04-03 10:00");
    reminder.setNama("makan");
    reminder.setId(1);
    reminder.setLineUser(lineUser);
    String namaReminder = reminder.getNama();
    Long idReminder = reminder.getId();

    commandRepository.newCommand(command);
    assertEquals(0,reminderService.getRemindTasks().size());
    String jawaban = reminderService.executeAdd("add",listAdd, lineUser, controller,waktu);
    assertEquals("ERROR: null", jawaban);
    verify(commandRepository, times(1)).executeAdd(command.nameCommand(), listAdd, lineUser);
  }

  @Test
  public void whenExecuteDeleteIsCalledItShouldCallCommandRepositoryExecuteDelete() throws ParseException {
    Command command = new DeleteCommand();
    String[] listDelete = {"delete","0"};

    assertEquals(0,reminderService.getRemindTasks().size());
    reminderService.executeAdd("add",listAdd, lineUser, controller,waktu);
    String jawaban = reminderService.executeDelete("delete", listDelete, lineUser);

    assertEquals(0,reminderService.getRemindTasks().size());
    lenient().when(reminderRepository.findReminderByName("makan",lineUser)).thenReturn(null);
    verify(commandRepository, times(1)).executeDelete(command.nameCommand(), listDelete, lineUser);
    assertEquals("Error: null", jawaban);
  }

  @Test
  public void whenExecuteListIsCalledItShouldCallCommandRepositoryExecuteList() throws ParseException {
    Command command = new ListCommand();
    Reminder reminder = new Reminder("makan","2020-04-03 10:00");
    reminder.setNama("makan");
    reminder.setId(1);
    reminder.setLineUser(lineUser);
    List<Reminder> listReminder = new ArrayList<>();
    listReminder.add(reminder);
    commandRepository.newCommand(command);
    String jawaban = reminderService.executeList("list",listListAll, lineUser);
    when(reminderRepository.findAllReminderByID(lineUser)).thenReturn(listReminder);
    assertEquals("Error: null", jawaban);
    verify(commandRepository, times(1)).executeList(command.nameCommand(),listListAll, lineUser);
  }

  @Test
  public void addByIdTest() {
    lenient().when(reminderService.addById(listAddById,lineUser,controller,waktu)).thenReturn("Reminder berhasil ditambahkan!");
    when(reminderRepository.findById((long) 1)).thenReturn(java.util.Optional.of(reminder));
    String answer = reminderService.addById(listAddById, lineUser,controller, waktu);
    assertEquals(0,reminderService.getRemindTasks().size());
    verify(reminderRepository, times(1)).getOne((long)1);
  }

  @Test
  public void setReminder() {
    when(reminderRepository.findById((long) 1)).thenReturn(java.util.Optional.of(reminder));
    String answer = reminderService.setReminder(listSetReminder, controller,waktu);
    assertEquals("Error: null", answer);
    verify(reminderRepository, times(1)).getOne((long)1);
  }

  @Test
  public void executeSnoozeTest(){
    String[] input = {"/snooze","1"};
    String jawaban = reminderService.executeSnooze(input,lineUser,controller,waktu);
    verify(reminderRepository, times(1)).getOne((long)1);
    assertEquals("Error: null", jawaban);
  }
}