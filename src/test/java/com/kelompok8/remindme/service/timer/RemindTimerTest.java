package com.kelompok8.remindme.service.timer;

import com.kelompok8.remindme.core.Reminder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.text.ParseException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

public class RemindTimerTest {
    private RemindTimer remindTimer;
    private Reminder reminder;

    @BeforeEach
    public void setUp() throws ParseException {
        this.reminder = new Reminder("Semangat Adprog","2020-05-11 15:30");
        this.remindTimer = new RemindTimer(reminder);
    }

    @Test
    public void testExist(){
        assertNotNull(this.remindTimer);
    }

    @Test
    public void testReminder(){
        assertEquals("Semangat Adprog",this.reminder.getNama());
    }

    @Test
    public void get() throws InterruptedException, ExecutionException {
        assertNotNull(this.remindTimer.get());
        assertTrue(this.remindTimer.get() instanceof CompletableFuture);

        String message = "! REMINDER: SEMANGAT ADPROG !";
        CompletableFuture<String> future = this.remindTimer.get();
        assertEquals(message,future.get());
    }
}
