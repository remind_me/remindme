package com.kelompok8.remindme.service.timer;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ScheduledCompletableTest {

  @Mock
  Supplier<CompletableFuture<String>> completable;

  @InjectMocks
  ScheduledCompletable scheduledCompletable;

  @Test
  public void schedule() {
    ScheduledExecutorService executor = Mockito.mock(ScheduledExecutorService.class);

    CompletableFuture<String> future = scheduledCompletable.schedule(executor, completable, 1);
    assertNotNull(future);
  }
}