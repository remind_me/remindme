package com.kelompok8.remindme.service.timer;

import com.kelompok8.remindme.controller.RemindmeController;
import com.kelompok8.remindme.core.Reminder;
import com.kelompok8.remindme.repository.ReminderRepository;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.time.Instant;
import java.util.concurrent.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class RemindTaskTest {
    @InjectMocks
    private RemindTask remindTask;

    @Mock
    RemindmeController remindmeController;

    @Mock
    ReminderRepository reminderRepository;

    @Mock
    ScheduledCompletable scheduledCompletable;

    @Mock
    ScheduledExecutorService scheduledExecutorService;

    private Reminder reminder;
    private Instant waktuDibuat;
    private CountDownLatch lock = new CountDownLatch(1);

    @BeforeEach
    public void setUp() throws ParseException {
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date defaultDate = dateFormatter.parse("2020-06-20 15:30");
        this.waktuDibuat = defaultDate.toInstant();

        Calendar cal = Calendar.getInstance();
        cal.setTime(defaultDate);
        cal.add(Calendar.MINUTE,1);
        String waktuRemind = dateFormatter.format(cal.getTime());

        this.reminder = new Reminder("Semangat",waktuRemind);
        this.remindTask = new RemindTask(remindmeController,reminder,reminderRepository);
    }

    @Test
    public void testExist(){
        assertNotNull(this.remindTask);
    }

    @Test
    public void testMakeRemindUnrepeat() throws Exception {
        assertEquals(0,this.remindTask.delay);
        assertEquals(0,this.remindTask.jumlahRemindMessage);

        this.remindTask.makeRemind(waktuDibuat);

        lock.await(1, TimeUnit.MINUTES);

        assertEquals(60000,this.remindTask.delay);
        assertEquals(1,this.remindTask.jumlahRemindMessage);
    }

    @Test
    public void testMakeRemindRepeat() throws Exception {
        this.reminder.setState("REPEATED");
        RemindTask task = new RemindTask(remindmeController,reminder,reminderRepository);
        assertEquals(0,task.delay);
        assertEquals(0,task.jumlahRemindMessage);

        task.makeRemind(waktuDibuat);

        lock.await(1, TimeUnit.MINUTES);

        assertEquals(60000,task.delay);
        assertEquals(1,task.jumlahRemindMessage);
    }

    @Test
    public void testFailedMakeRemind() {
        Exception exception = assertThrows(Exception.class, () -> {
            DateTime dt = new DateTime(2020, 6, 23, 10, 0, 0, 0);
            Instant waktuDibuat = Instant.ofEpochMilli(dt.toInstant().getMillis());
            DateTime waktuRemind = dt.minusMinutes(1);
            DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");

            this.reminder = new Reminder("Semangat",dtf.print(waktuRemind));
            this.remindTask.makeRemind(waktuDibuat);
        });

        String expectedMessage = "The due date of the reminder is in the past";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testCancelRemind() throws Exception{
        this.remindTask.makeRemind(waktuDibuat);
        this.remindTask.cancelRemind();
        assertTrue(this.remindTask.getScheduledExecutorService().isShutdown());
    }
}
