package com.kelompok8.remindme.service;

import com.kelompok8.remindme.controller.RemindmeController;
import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.core.ReminderPackage;
import com.kelompok8.remindme.repository.ReminderPackageRepository;
import com.kelompok8.remindme.repository.ReminderRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReminderPackageServiceImplementationTest {

  @InjectMocks
  private ReminderPackageServiceImplementation reminderPackageServiceImplementation;

  @Mock
  private ReminderPackageRepository reminderPackageRepository;

  @Mock
  private ReminderRepository reminderRepository;

  @Mock
  private RemindmeController remindmeController;

  @AfterEach
  void tearDown() {
    reset(reminderPackageRepository);
    reset(reminderRepository);
  }

  private Instant waktu;

  @BeforeEach
  public void setUp() throws ParseException {
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    Date defaultDate = dateFormatter.parse("2020-05-11 15:30");
    this.waktu = defaultDate.toInstant();
  }

  @Test
  void addReminderFromPkgSuccess() {
    ReminderPackage reminderPackage = new ReminderPackage(
            "Work Out Package",
            "Push up, 08:30; Sit up, 08:40; Plank, 08:50",
            "https://images.unsplash.com/photo-1584735935682" +
                    "-2f2b69dff9d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEy" +
                    "MDd9&auto=format&fit=crop&w=751&q=80");
    reminderPackage.setId(1);

    LineUser lineUser = new LineUser("1","name");

    when(reminderPackageRepository.findById((long) 1)).thenReturn(java.util.Optional.of(reminderPackage));

    String jawaban = reminderPackageServiceImplementation.addReminderFromPackage(1,lineUser,remindmeController,waktu, "2020-08-15");
//    assertEquals("Berhasil menambahkan Work Out Package!", jawaban);
    verify(reminderPackageRepository, times(1)).findById((long) 1);
  }

  @Test
  void addReminderFromPkgFailNullPointerException() {
    ReminderPackage reminderPackage = new ReminderPackage("Work Out",
            "Push up, 08:30; Sit up, 08:40; Plank, 08:50",
            "https://images.unsplash.com/photo-1584735935682" +
                    "-2f2b69dff9d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEy" +
                    "MDd9&auto=format&fit=crop&w=751&q=80");
    reminderPackage.setId(1);

    LineUser lineUser = new LineUser("1","name");

    when(reminderPackageRepository.findById((long) 2)).thenReturn(null);

    String jawaban = reminderPackageServiceImplementation.addReminderFromPackage(2,lineUser,remindmeController,waktu, "2020-08-15");
    assertEquals("Package tidak dapat ditemukan.", jawaban);
    verify(reminderPackageRepository, times(1)).findById((long) 2);
  }

  @Test
  void addReminderFromPkgFailIndexOutOfBoundException() {
    ReminderPackage reminderPackage = new ReminderPackage("Work Out",
            "Push up, 08:30; Sit up; aa:40; Plank, 08:50",
            "https://images.unsplash.com/photo-1584735935682" +
                    "-2f2b69dff9d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEy" +
                    "MDd9&auto=format&fit=crop&w=751&q=80");
    reminderPackage.setId(1);

    LineUser lineUser = new LineUser("1","name");

    when(reminderPackageRepository.findById((long) 1)).thenReturn(java.util.Optional.of(reminderPackage));

    String jawaban = reminderPackageServiceImplementation.addReminderFromPackage(1,lineUser,remindmeController,waktu, "2020-08-15");
//    assertEquals("Ada kesalahan pada package.", jawaban);
    verify(reminderPackageRepository, times(1)).findById((long) 1);
  }


  @Test
  void getPackageDesc() {
    ReminderPackage reminderPackage = new ReminderPackage(
            "Work Out Package",
            "Push up, 08:30; Sit up, 08:40; Plank, 08:50",
            "https://images.unsplash.com/photo-1584735935682" +
                    "-2f2b69dff9d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEy" +
                    "MDd9&auto=format&fit=crop&w=751&q=80");
    reminderPackage.setId(1);

    when(reminderPackageRepository.findById((long) 1)).thenReturn(java.util.Optional.of(reminderPackage));
    assertEquals("Work Out Package\n\n"
                    +"List reminder:\n"
                    +"1. 08:30 - Push up\n"
                    +"2. 08:40 - Sit up\n"
                    +"3. 08:50 - Plank",
            reminderPackageServiceImplementation.getPackageDesc(1));
  }

  @Test
  void isLastReminderOfPackageTrue () {
    assertEquals(true, reminderPackageServiceImplementation.isLastReminderInPackage(2,2));
  }

  @Test
  void isLastReminderOfPackageFalse () {
    assertEquals(false, reminderPackageServiceImplementation.isLastReminderInPackage(1,2));
  }

  @Test
  void getReminderDesc () {
    assertEquals("1. 08:30 - Push up\n", reminderPackageServiceImplementation.getReminderDesc("Push up, 08:30", 1, 2));
    assertEquals("1. 08:30 - Push up", reminderPackageServiceImplementation.getReminderDesc("Push up, 08:30", 1, 1));
  }

  @Test
  void getReminderPackageById() {
    ReminderPackage reminderPackage = new ReminderPackage(
            "Work Out",
            "Push up, 08:30; Sit up, 08:40; Plank, 08:50",
            "https://images.unsplash.com/photo-1584735935682" +
                    "-2f2b69dff9d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEy" +
                    "MDd9&auto=format&fit=crop&w=751&q=80");
    reminderPackage.setId(1);

    when(reminderPackageRepository.findById((long) 1)).thenReturn(java.util.Optional.of(reminderPackage));

    assertEquals(java.util.Optional.of(reminderPackage), reminderPackageServiceImplementation.getReminderPackageById((long)1));
    verify(reminderPackageRepository, times(1)).findById((long) 1);
  }

  @Test
  void getListReminderPackage() {
    ReminderPackage reminderPackage1 = new ReminderPackage(
            "Work Out Package",
            "Push up, 08:30; Sit up, 08:40; Plank, 08:50",
            "https://images.unsplash.com/photo-1584735935682" +
                    "-2f2b69dff9d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEy" +
                    "MDd9&auto=format&fit=crop&w=751&q=80");
    reminderPackage1.setId(1);
    ReminderPackage reminderPackage2 = new ReminderPackage(
            "Dog People Package",
            "Play with your dog(s), 09.30; Feed your dog(s), 10.00; Nap time!!, 13.00",
            "https://images.unsplash.com/photo-1554693190-383dd5302125?ixlib=rb-1.2." +
                    "1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=751&q=80"
    );
    reminderPackage2.setId(2);

    String expectedListReminderPackage =
            "List Reminder Package:\n"
            + "1. Work Out Package (ID = 1)\n"
            + "2. Dog People Package (ID = 2)\n";

    List<ReminderPackage> listOfReminderPackage = new ArrayList<ReminderPackage>();
    listOfReminderPackage.add(reminderPackage1);
    listOfReminderPackage.add(reminderPackage2);
    when(reminderPackageRepository.findAll()).thenReturn(listOfReminderPackage);
    assertEquals(expectedListReminderPackage, reminderPackageServiceImplementation.getListReminderPackage());
    verify(reminderPackageRepository, times(1)).findAll();
  }
}
