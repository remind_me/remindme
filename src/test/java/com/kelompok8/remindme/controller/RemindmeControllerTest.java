package com.kelompok8.remindme.controller;

import com.kelompok8.remindme.core.HelpFlex;
import com.kelompok8.remindme.core.LineUser;
import com.kelompok8.remindme.service.ReminderPackageServiceImplementation;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.message.template.CarouselTemplate;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class RemindmeControllerTest {

  @Mock
  CarouselTemplate carouselTemplate;

  @Mock
  LineMessagingClient lineMessagingClient;

  @Mock
  ReminderPackageServiceImplementation reminderPackageServiceImplementation;

  @InjectMocks
  RemindmeController controller;

  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final PrintStream originalOut = System.out;

  @BeforeEach
  public void setUp() {
    System.setOut(new PrintStream(outContent));
  }

  @After
  public void restoreStreams() {
    System.setOut(originalOut);
  }

  @Test
  public void handleTextEvent() throws ExecutionException, InterruptedException, ParseException {
    String answer;

    MessageEvent<TextMessageContent> request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/showmenu"),
        Instant.now()
    );

    String input = request.getMessage().getText().toLowerCase();
    String[] splitInput = input.split(";");
    String firstWordInput = splitInput[0];
    assertEquals("/showmenu", input);
    assertEquals("/showmenu", firstWordInput);

    controller.handleTextEvent(request);

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/addreminder"),
        Instant.now()
    );
    controller.handleTextEvent(request);

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/deletereminder"),
        Instant.now()
    );
    controller.handleTextEvent(request);

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/showreminder"),
        Instant.now()
    );
    controller.handleTextEvent(request);

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/addbyId"),
        Instant.now()
    );
    controller.handleTextEvent(request);

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/package"),
        Instant.now()
    );
    controller.handleTextEvent(request);

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/allpackage"),
        Instant.now()
    );
    controller.handleTextEvent(request);

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/packagedesc;1"),
        Instant.now()
    );
    controller.handleTextEvent(request);

    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date defaultDate = dateFormatter.parse("2020-06-20 15:30:00");
    Instant time = defaultDate.toInstant();

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/addpackage;1;2020-06-26"),
        time
    );
    LineUser lineUser = new LineUser("userId","userName");
    controller.handleTextEvent(request);

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "/snoozereminder"),
        Instant.now()
    );
    controller.handleTextEvent(request);

    request = new MessageEvent<TextMessageContent>(
        "replyToken",
        new UserSource("userId"),
        new TextMessageContent("id", "5"),
        Instant.now()
    );
    answer = "Sorry, invalid command\n" +
        "Type /showMenu for valid commands";
    controller.handleTextEvent(request);
  }

  @Test
  public void handleReplyEvent() {
    final String replyToken = "replyToken";
    final String answer = "Sorry it's still under construction";
    controller.handleReplyEvent(replyToken, answer);
    assertEquals("ERROR: null\n", outContent.toString());
  }

  @Test
  public void handleReplyFlex() {
    final String replyToken = "replyToken";
    controller.handleReplyFlexEvent(replyToken, new HelpFlex().get());
    assertEquals("ERROR: null\n", outContent.toString());
  }

  @Test
  public void handlePushEvent() {
    final String userID = "userID";
    final String message = "Reminder Notification: Workout!";
    controller.handlePushEvent(userID, message);
    assertEquals("ERROR: null\n", outContent.toString());
  }

  @Test
  public void handlePushEventTemplateMessage() throws Exception {
    final String userID = "userID";
    final TemplateMessage message = new TemplateMessage("Reminder Packages", carouselTemplate);
    controller.handlePushEventTemplateMessage(userID, message);
    assertEquals("ERROR: null\n", outContent.toString());
  }

  @Test
  public void TestMakeCarouselColList(){
    long[] arrPackageId = {1, 2, 3, 4, 5};
    controller.makeCarouselColList(arrPackageId);
    assertEquals("ERROR: No value present\n", outContent.toString());
  }
}