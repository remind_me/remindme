package com.kelompok8.remindme.core;

import com.kelompok8.remindme.repository.ReminderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class UnrepeatedYetStateTest {

    @InjectMocks
    UnrepeatedYetState unrepeatedYetState;

    @Mock
    Reminder set;

    @Mock
    ReminderRepository reminderRepository;

    @BeforeEach
    void setUp() {
        this.unrepeatedYetState = new UnrepeatedYetState(set,reminderRepository);
        unrepeatedYetState.repeat();
    }

    @Test
    void repeat() {
        unrepeatedYetState.repeat();
        assertEquals("Success! This reminder will be reminded once every hour.", unrepeatedYetState.repeat());
    }

    @Test
    void unrepeat() {
        unrepeatedYetState.unrepeat();
        assertEquals("This reminder has already been set up as Unrepeated", unrepeatedYetState.unrepeat());
    }

    @Test
    void testToString() {
        assertEquals("Unrepeated Reminder", unrepeatedYetState.toString());
    }
}