package com.kelompok8.remindme.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.*;
import com.kelompok8.remindme.core.ReminderState;
import com.kelompok8.remindme.core.RepeatedState;
import com.kelompok8.remindme.core.UnrepeatedYetState;

public class StateHelperTest {
    @Autowired
    private RepeatedState repeatedState;

    @Autowired
    private UnrepeatedYetState unrepeatedYetState;

    StateHelper stateHelper;

    @BeforeEach
    void setUp() {
        stateHelper = new StateHelper();
    }

    @Test
    void toState() {
        assertEquals(repeatedState, stateHelper.toState(RepeatedState.DB_COL_NAME));

        assertEquals(unrepeatedYetState, stateHelper.toState(UnrepeatedYetState.DB_COL_NAME));

        assertNull(stateHelper.toState("test"));
    }
}
