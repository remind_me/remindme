package com.kelompok8.remindme.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ReminderPackageTest {

  private ReminderPackage reminderPackage;

  @BeforeEach
  public void setUp(){
      reminderPackage = new ReminderPackage(
              "Work Out",
              "Push up, 08:30; Sit up, 08:40; Plank, 08:50",
              "https://images.unsplash.com/photo-1584735935682" +
                      "-2f2b69dff9d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEy" +
                      "MDd9&auto=format&fit=crop&w=751&q=80");
      reminderPackage.setId(1);
  }

  @Test
  void getNama() {
      assertEquals("Work Out", reminderPackage.getNama());
  }

  @Test
  void setNama() {
      reminderPackage.setNama("Senam");
      assertEquals("Senam", reminderPackage.getNama());
  }

  @Test
  void getId() {
      assertEquals(1,reminderPackage.getId());
  }

  @Test
  void setId() {
      reminderPackage.setId(2);
      assertEquals(2, reminderPackage.getId());
  }

  @Test
  void getList() {
      assertEquals("Push up, 08:30; Sit up, 08:40; Plank, 08:50", reminderPackage.getListReminder());
  }

  @Test
  void setList() {
      reminderPackage.setListReminder("Pull up, 08:30; Sit up, 08:40; Plank, 08:50");
      assertEquals("Pull up, 08:30; Sit up, 08:40; Plank, 08:50", reminderPackage.getListReminder());
  }

  @Test
  void getImageUrl() {
    assertEquals("https://images.unsplash.com/photo-1584735935682" +
            "-2f2b69dff9d2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEy" +
            "MDd9&auto=format&fit=crop&w=751&q=80", reminderPackage.getImageUrl());
  }

  @Test
  void setImageUrl() {
    reminderPackage.setImageUrl("https://images.unsplash.com/photo-1554693190" +
            "-383dd5302125?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format" +
            "&fit=crop&w=751&q=80");
    assertEquals("https://images.unsplash.com/photo-1554693190" +
            "-383dd5302125?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format" +
            "&fit=crop&w=751&q=80", reminderPackage.getImageUrl());
  }

}

