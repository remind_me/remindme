package com.kelompok8.remindme.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.message.FlexMessage;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class HelpFlexTest {

  private static final ObjectMapper objectMapper =
      ModelObjectMapper
          .createNewObjectMapper()
          .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

  @Test
  public void flexMessage() throws Exception {
    final FlexMessage flexMessage = new HelpFlex().get();
    final Object reconstructed = serializeThenDeserialize(flexMessage);
    assertEquals(reconstructed,flexMessage);
  }

  Object serializeThenDeserialize(final Object original) throws Exception {
    final String asJson = objectMapper.writeValueAsString(original);
    final Object reconstructed = objectMapper.readValue(asJson, original.getClass());
    return reconstructed;
  }
}
