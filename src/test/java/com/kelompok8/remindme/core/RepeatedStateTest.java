package com.kelompok8.remindme.core;

import com.kelompok8.remindme.repository.ReminderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class RepeatedStateTest {

    @InjectMocks
    RepeatedState repeatedState;

    @Mock
    Reminder set;

    @Mock
    ReminderRepository reminderRepository;

    @BeforeEach
    void setUp() {
        this.repeatedState = new RepeatedState(set,reminderRepository);
        repeatedState.repeat();
    }

    @Test
    void repeat() {
        repeatedState.repeat();
        assertEquals("This reminder has already been set up as Repeated", repeatedState.repeat());
    }

    @Test
    void unrepeat() {
        repeatedState.unrepeat();
        assertEquals("Success! This reminder will only be reminded once.", repeatedState.unrepeat());
    }

    @Test
    void testToString() {
        assertEquals("Repeated Reminder", repeatedState.toString());
    }
}
