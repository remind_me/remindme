package com.kelompok8.remindme.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class ReminderTest {
    private Reminder reminder;
    private LineUser lineUser;

    @BeforeEach
    public void setUp() throws ParseException {
        lineUser = new LineUser("1","nama");
        reminder = new Reminder("Work Out", "2020-08-15 15:00");
        reminder.setLineUser(lineUser);
        reminder.setId(1);
    }

    @Test
    void getNama() {
        assertEquals("Work Out", reminder.getNama());
    }

    @Test
    void setNama() {
        reminder.setNama("Senam");
        assertEquals("Senam", reminder.getNama());
    }

    @Test
    void getId() {
        assertEquals(1,reminder.getId());
    }

    @Test
    void setId() {
        reminder.setId(2);
        assertEquals(2, reminder.getId());
    }

    @Test
    void getWaktu() throws ParseException {
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = dateFormatter.parse("2020-08-15 15:00");
        assertEquals(date, reminder.getWaktu());
    }

    @Test
    void setWaktu() throws ParseException {
        reminder.setWaktu("2020-08-15 16:00");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = dateFormatter.parse("2020-08-15 16:00");
        assertEquals(date, reminder.getWaktu());
    }

    @Test
    void getUserId() {
        assertEquals("1", reminder.getLineUser().getUserId());
    }

    @Test
    void setUserId() {
        lineUser.setUserId("2");
        assertEquals("2", reminder.getLineUser().getUserId());
    }

    @Test
    public void testrepeat() throws ParseException {
        assertEquals("UNREPEATED_YET", reminder.getState());
        assertEquals("Sekarang UNREPEATED_YET.", reminder.toString());
    }

//    @Test
//    void testReminderRepeat() throws ParseException {
//        System.out.println("Repeat Test");
//        Reminder set = new Reminder();
//        System.out.println(set);
//        set.repeat();
//        System.out.println(set);
//
//        System.out.println("Unrepeat Test");
//        System.out.println(set);
//        set.unrepeat();
//        System.out.println(set);
//    }
}