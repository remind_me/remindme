package com.kelompok8.remindme.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class LineUserTest {

  LineUser lineUser;

  @BeforeEach
  public void setUp() throws ParseException {
    lineUser = new LineUser("thamiID","thami");
  }

  @Test
  public void construct() {
    LineUser user = new LineUser();
    assertNull(user.getUserId());
    assertNull(user.getUserName());
  }

  @Test
  public void getUserId() {
    assertEquals("thamiID",lineUser.getUserId());
  }

  @Test
  public void getUserName() {
    assertEquals("thami",lineUser.getUserName());
  }

  @Test
  public void setUserId() {
    lineUser.setUserId("thamiNewID");
    assertEquals("thamiNewID",lineUser.getUserId());
  }

  @Test
  public void setUserName() {
    lineUser.setUserName("thamiNewName");
    assertEquals("thamiNewName",lineUser.getUserName());
  }
}
