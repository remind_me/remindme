**RemindMe Bot 1.0 - Monolithic Architecture**

[![pipeline status](https://gitlab.com/remind_me/remindme/badges/master/pipeline.svg)](https://gitlab.com/remind_me/remindme/-/commits/master)
[![coverage report](https://gitlab.com/remind_me/remindme/badges/master/coverage.svg)](https://gitlab.com/remind_me/remindme/-/commits/master)


**RemindMe Bot 2.0 - Microservice Architecture**

Reminder Service
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[![pipeline status](https://gitlab.com/remind_me/remindme/badges/reminder-service/pipeline.svg)](https://gitlab.com/remind_me/remindme/-/commits/reminder-service)
[![coverage report](https://gitlab.com/remind_me/remindme/badges/reminder-service/coverage.svg)](https://gitlab.com/remind_me/remindme/-/commits/reminder-service)


Reminder Package Service
&nbsp;[![pipeline status](https://gitlab.com/remind_me/remindme/badges/reminderpackage-service/pipeline.svg)](https://gitlab.com/remind_me/remindme/-/commits/reminderpackage-service)
[![coverage report](https://gitlab.com/remind_me/remindme/badges/reminderpackage-service/coverage.svg)](https://gitlab.com/remind_me/remindme/-/commits/reminderpackage-service)


Line User Service
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[![pipeline status](https://gitlab.com/remind_me/remindme/badges/lineuser-service/pipeline.svg)](https://gitlab.com/remind_me/remindme/-/commits/lineuser-service)
[![coverage report](https://gitlab.com/remind_me/remindme/badges/lineuser-service/coverage.svg)](https://gitlab.com/remind_me/remindme/-/commits/lineuser-service)



## Tentang Kami

Anggota Kelompok **Kelas A Kelompok 8** :
- Jonathan - 1806204985
- Kukuh Hafiyyan - 1806186616
- Putri Salsabila - 1806186805
- Thami Endamora - 1806141460

## Tentang remindMe

remindMe adalah sebuah Line chatbot yang bertujuan untuk mengingatkan agenda-agenda pengguna dalam waktu tertentu.


## Daftar Fitur

- Creations (Dikerjakan oleh Kukuh Hafiyyan)
- Notifications & Snooze (Dikerjakan oleh Thami Endamora)
- Share & Repeat (Dikerjakan oleh Putri Salsabila)
- Package (Dikerjakan oleh Jonathan)